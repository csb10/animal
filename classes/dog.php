<?php
include_once("classes/animal.php");

class Dog extends Animal {

	public function __construct($properties = null) {
		parent::__construct($properties);
	}

	protected function makeNoise() {
		return "Woof!!";
	}

	public function bark() {
		return $this->makeNoise();
	}

}