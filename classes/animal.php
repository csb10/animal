<?php

abstract class Animal {
	private $legs;
	private $coat;
	private $eyes;
	private $height;
	private $weight;
	private $gender;
	private $name;
	protected $noise;

	public function __construct($properties = null) {
		if (isset($properties)) {
			$this->setLegs($properties['legs']);
			$this->setCoat($properties['coat']);
			$this->setEyes($properties['eyes']);
			$this->setHeight($properties['height']);
			$this->setWeight($properties['weight']);
			$this->setGender($properties['gender']);
			$this->setName($properties['name']);
			return $this;
		}
	}

	public function setLegs($legs) {
		$this->legs = $legs;
	}

	public function getLegs() {
		return $this->legs;
	}

	public function setCoat($coat) {
		$this->coat = $coat;
	}

	public function getCoat() {
		return $this->coat;
	}

	public function setEyes($eyes) {
	 	$this->eyes = $eyes;
	}

	public function getEyes() {
		return $this->eyes;
	}

	public function setHeight($height) {
		$this->height = $height;
	}

	public function getHeight() {
		return $this->height;
	}

	public function setWeight($weight) {
		$this->weight = $weight;
	}

	public function getWeight() {
		return $this->weight;
	}

	public function setGender($gender) {
		$this->gender = $gender;
	}

	public function getGender() {
		return $this->gender;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setNoise($noise) {
		$this->noise = $noise;
	}

	public function getNoise() {
		return $this->noise;
	}

	abstract protected function makeNoise();
}