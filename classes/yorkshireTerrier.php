<?php
include_once("classes/dog.php");

class YorkshireTerrier extends Dog {

	public function __construct($properties = null) {
		parent::__construct($properties);
	}

	protected function makeNoise() {
		return "Yapp!";
	}
}