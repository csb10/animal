<?php
include_once("classes/yorkshireTerrier.php");
include_once("classes/westHighlandWhiteTerrier.php");

$newDogs = array(
	array(
		"type" => "westHighlandWhiteTerrier",
		"legs" => "2",
		"coat" => "black",
		"name" => "Alfie",
		"eyes" => "Blue",
		"height" => "1m",
		"weight" => "1st",
		"gender" => "Male",
	),

	array(
		"type" => "yorkshireTerrier",
		"legs" => "4",
		"coat" => "red",
		"name" => "Reddie",
		"eyes" => "Orange",
		"height" => "10m",
		"weight" => "10st",
		"gender" => "Female",
	),
);

foreach ($newDogs as $newDog) {

	$dog = new $newDog['type']($newDog);

	echo "My name is '".$dog->getName()."' hear me '".$dog->bark()."'<br/>";

	echo $dog->getLegs()."<br/>";
	echo $dog->getCoat()."<br/>";
	echo $dog->getName()."<br/>";
	echo $dog->getEyes()."<br/>";
	echo $dog->getHeight()."<br/>";
	echo $dog->getWeight()."<br/>";
	echo $dog->getGender()."<br/>";
}



